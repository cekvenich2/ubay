Infiniti Energy is an innovative, full-service solar power developer headquartered in New Jersey. The company specializes in the development and acquisition of commercial and industrial solar assets in both the src and private sector nationwide. The company targets installations predominantly in the New England and Mid-Atlantic regions of the United States.  The Company also offers complete turnkey EPC services for commercial, industrial and municipal clients from initial concept to fully energized photovoltaic systems.


[Infinitienergy.com](http://infinitienergy.com)
