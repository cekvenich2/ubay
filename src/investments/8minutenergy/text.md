8minute Solar Energy is a leading utility scale solar and battery storage company with a strong track record of developing projects and a large pipeline of advanced stage projects across attractive solar markets in the United States. Solar is increasingly becoming the cheapest form of power generation across the Southeast and Southwest U.S., and benefits from strong government support. The investment was made by Upper Bay in partnership with its co-investors J.P. Morgan Asset Management and the University of California (UC) Office of the Chief Investment Officer of the Regents.


[8minutenergy.com](https://www.8minutenergy.com/)
