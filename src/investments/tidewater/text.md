Tidewater, founded in 1932, has evolved into a multi-commodity transportation and terminal company serving the diverse and evolving transportation needs of the Pacific Northwest. Tidewater is an essential infrastructure provider with high barriers to entry due to permitting, scale requirements, and regulatory barriers (i.e. Jones Act). The investment was made in partnership with Blackrock, Ullico, Silverfern and a U.S. pension fund.


[Tidewater.com](http://www.tidewater.com/)
