
//https://cdn.jsdelivr.net/npm/instant.page.es5@2.0.0/instantpage.es5.js


// FOUT section
function loadFonts(fontsArr) {
      var fontConfig = {
        classes: false,
        google: {
          families: fontsArr
        }
      }
      WebFont.load(fontConfig)
}
// END FOUT section

//- eg addScript('bla.js', null, 'api-key', 'key123') when they want you to use the tag: so you can in your own sequence
function addScript(src, callback, attr, attrValue, id) {
    var s = document.createElement('script')
    s.setAttribute('src', src)
    if (attr) s.setAttribute(attr, attrValue)
    if (id) s.id = id
    if (callback) s.onload = callback
    s.async = true // it does it anyway, as the script is async
    document.getElementsByTagName('body')[0].appendChild(s)
}


// get style value
function getStyle(el, styleProp) {
    if (el.currentStyle)
      var y = el.currentStyle[styleProp];
    else if (window.getComputedStyle)
      var y = document.defaultView.getComputedStyle(el, null).getPropertyValue(styleProp)
    return y;
  }
  

function renderMustache(root, id, data) {
    let template = root.getElementById(id).innerHTML
    return Mustache.render(template, data)
}
  

function inView(el) { // is element in view?
    //special bonus for jQuery
    if (typeof jQuery === 'function' && el instanceof jQuery) {
      el = el[0]
    }
    var rect = el.getBoundingClientRect()
    return (
      rect.top >= 0 &&
      rect.left >= 0 &&
      rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
      rect.right <= (window.innerWidth || document.documentElement.clientWidth)
    )
  }